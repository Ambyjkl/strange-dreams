// var a
// a = require('vuepress-theme-craftdocs')
// throw new Error(a)

const path = __dirname + '/../chapters'
const fs = require('fs')
const chapters = fs.readdirSync(path).map(each => `/chapters/${each}`)
const { PLUGINS, REQUIRED_PLUGINS } = require('@vuepress/markdown/lib/constant.js')
REQUIRED_PLUGINS.push(PLUGINS.CONTAINERS)

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

module.exports = {
  title: 'Strange Dreams',
  base: '/strange-dreams/',
  description: "When you're so bored you decide to write a book",
  head: [
    ['meta', { name: 'theme-color', content: '#de2d26' }],
    ['link', { rel: 'canonical', href: 'https://ambyjkl.gitlab.io/strange-dreams' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['link', { rel: 'preconnect', href: 'https://www.google-analytics.com/' }],
  ],
  plugins: {
    '@vuepress/pwa': {
      serviceWorker: true,
      updatePopup: {
        message: 'New content is available!',
        buttonText: 'Refresh',
      },
    },
    '@vuepress/google-analytics': {
      ga: 'UA-90754770-2',
    },
    '@vuepress/plugin-medium-zoom': {}
  },
  themeConfig: {
    sidebar: [
      {
        title: 'Table of Contents',
        collapsable: false,
        children: [
          '/preamble',
          ...chapters,
        ],
      },
    ],
    nav: [
      { text: 'Main Website', link: 'https://ambyjkl.tech' },
      { text: 'GitLab', link: 'https://gitlab.com/Ambyjkl/strange-dreams' },
    ],
    // lastUpdated: true,
  },
  markdown: {
    chainMarkdown (config) {
      require('@vuepress/markdown').removeAllBuiltInPlugins(config)
      config.options.highlight(null)
    },
  },
  // chainWebpack (config, isServer) {
  //   !isServer && config.plugin('BundleAnalyzer').use(BundleAnalyzerPlugin)
  // },
  evergreen: true,
}
