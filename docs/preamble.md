# Preamble

::: danger
DO NOT SKIP THIS, THIS IS PROBABLY THE BEST PREAMBLE YOU'RE EVER GOING TO READ.
:::

## The Motivation

A book about dreams huh? Are you kidding me? Wait, I'll tell you why. I've always wanted to write a book, but it kind of seemed like an impossible task for me and I've laughed it off as something I'll never find myself doing. But, I think I've finally found the best book genre (style/class, what do you call this?) I can possibly write. But before I get into that, let me first explain why the other ones are not a fit for me (aka me shitting on every kind of book).

### Non-Fiction

#### Based on real-life incidents
My real life is too boring to write about that I've never even thought about it. About other people's real life, nah, I don't care enough.

#### Informative (like about something useful, like technical stuff, How to Read a Person Like a Book, okay not the last one)
Let's be honest. I know enough random trivia in the most unexpected topics (which means exclude things real people know about like sports and movies) to annoy people I know of, and also some knowledge in things that I care about IRL (like coding, programming, software development, typing random words in a language loosely resembling English to make an electrified rock obey my commands) but not complete or factually correct enough that I can impose my opinion on others by writing about it (and not give nightmares and early death to people who listen to me after reading my stuff). Hence, I have no choice but to rule out books that actually help society be a better place.

#### <insert the other kinds here once I've thought about them>
This is section is intentionally left blank (I never really got why they have these things in books)

### Fiction

#### Novels
HAHAHA. I've hardly had the patience to sit through a single one of these atrocities that I don't see myself comfortable writing one. Also, there are too many of these, and I don't want to burden this planet with another.

#### Comics
If I could draw ;_;

#### Any book that has some kind of story (including above and below)
A story is something that is not meant to be taken lightly. It has several parts: (these are just two of them because I'm lazy, and I know you don't care either)

- Plot:
Plot is hard. It's convoluted, complicated. I cannot visualize the entire scene from beginning to end because I'm a normal human being. Even if I come up with something, it's 100% guaranteed to make zero sense, and halfway through I would see how pathetic my imagination is, I would roll up on the ground, try not to cry, cry a lot, then go back to browsing memes after I delete everything. Even if it ends up getting through, someone's going to come around and tell me about how my story has 10 million plotholes when the whole thing is only ten pages long. Then, out of guilt, I'd have to dedicate the rest of my life writing sequels that address said plotholes, as well as creating new ones that would be solved later, proceeding in a downward spiral of doom.

- Characters:
Yeah, like the potter who happens to be a magician. Characters are hard, and, due to similar reasons as above, it's hard to have strong characters with solid backstories and motivations when the author doesn't give a damn to begin with.

#### Collection of short stories
Simple stories, easy characters, no continuity, no chance of plot holes, awesome right? Wrong, because with an already horrible sense of imagination there's no way I'm thinking of new settings, new characters, new everything over and over again. But this seems to have some potential, let's keep this for later.

#### Poetry
You're kidding, right?

## The Epiphany

As I've been thinking of this stuff, I've been having dreams, interesting dreams, stupid dreams. Then I finally realized, I've had it with me all along! The ultimate book genre (that's what you call these things right?), uninhibited by any of the above limitations: a collection of dreams.

What a legend, I'm proud of me! Each chapter is just `head -n 10 /dev/urandom > chapter.md`, some [pseudo]random (since nothing is ever truly random; it's just a mashup of memories) rubbish my brain cooks up while I'm asleep. Plot is nonexistent, nothing makes sense, ever, characters are usually people I know IRL (yeah, I do know people IRL, no matter how surprising that sounds). Everything is so bad that it is actually good.

Wow, it took me 20 long years to figure out something this simple? Goddamnit, all the dreams over the years I could have had in here. Should have listened to mom and dad when they told me to write a diary when I was 5.

___

If you have made it down here without skipping, congratulations! I'll give you cheat code that will enhance your experience reading this book. Just kidding, cheating is not good, people. I can guarantee this book is 100% cheating-proof, and also Pluto should be part of the Solar System; I hate Neil deGrasse Tyson for removing it, and your mom is fat, and I don't know what I'm even saying at this point. It's almost 4 AM I'm sleepy.
