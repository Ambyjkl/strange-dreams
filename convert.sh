#! /bin/sh

cd $1
convert -strip $2 -resize 512x 512.webp
convert -strip -interlace Plane $2 -resize 512x 512.jpg
convert -strip $2 -resize 740x 1.webp
convert -strip -interlace Plane $2 -resize 740x 1.jpg
convert -strip $2 -resize 1110x 1.5.webp
convert -strip -interlace Plane $2 -resize 1110x 1.5.jpg
convert -strip $2 -resize 1480x 2.webp
convert -strip -interlace Plane $2 -resize 1480x 2.jpg
